﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bonus1.aspx.cs" Inherits="BonusAssignment.Bonus1" %>

<!DOCTYPE html>
<!-- Bonus Question 2 -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script runat="server">

             public void CheckifPrime(object sender, EventArgs args)
             {
             string msg = "";
             string input = (String.Format("{0}", Request.Form["number"]));
             if(String.IsNullOrEmpty(input)) { input = "0"; }
             int num_input = int.Parse(input);

             for (int i = num_input - 1; i>1; i--)
             {
                 if ((num_input % i) == 0)
                 {
                     msg = "The number is not prime, because it is divisible by " + i;
                     break;
                 }
                 else if (i==2)
                 {
                     msg = "The number is prime.";
                 }
             }
             outputMessage.Text = msg;

             }
        </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <asp:Label runat="server" > Divisible  : </asp:Label>
            <asp:TextBox runat="server" id="number" placeholder="Write number"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="reqnumber" ControlToValidate="number" ValidationExpression = "^[1-9]\d*(\.\d+)?$" ErrorMessage="you must enter a number"></asp:RegularExpressionValidator>
            <br />
            <asp:Button id="submit" runat="server" Text="Submit" OnClick="CheckifPrime" />
        </div>
        <div>
            <asp:Label runat="server" id="outputMessage" />
        </div>
    </form>

   

</body>
</html>
