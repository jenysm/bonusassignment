﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bonus2.aspx.cs" Inherits="BonusAssignment.Bonus2" %>

<!DOCTYPE html>
<!-- Bonus Question 3 -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script runat="server">
        public void CheckForPalindrome(object sender, EventArgs args)
        {
            string msg = "";
            string inputString = palindromes.Text.ToString();
            inputString=inputString.ToLower();
            inputString=inputString.Replace(" ", null);
            char[] arr = inputString.ToCharArray();
            Array.Reverse(arr);

            string a = new string(arr);

            if (inputString == a){
                msg = "This string is a palindrome.";
            }else{
                msg = "Try another word.";
            }

            outputMessage.Text = msg;
        }


    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <asp:Label runat="server" >Palindromes : </asp:Label>
            <asp:TextBox runat="server" ID="palindromes" name="palindromes" placeholder="Write a string"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="req" ControlToValidate="palindromes" ErrorMessage="you must enter a string"></asp:RequiredFieldValidator>
            <br />
            <asp:Button id="submit" runat="server" Text="Submit" OnClick="CheckForPalindrome" />
        </div>
        <div>
            <asp:Label runat="server" id="outputMessage" />
        </div>
    </form>

   

</body>
</html>
