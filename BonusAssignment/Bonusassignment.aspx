﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bonusassignment.aspx.cs" Inherits="BonusAssignment.Bonusassignment" %>

<!DOCTYPE html>
<!-- Bonus Question 1 -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            string output = "";
            string strX = (String.Format("{0}", Request.Form["xValue"]));
            string strY = (String.Format("{0}", Request.Form["yValue"]));

            if(String.IsNullOrEmpty(strX)) { strX = "0"; }
            if(String.IsNullOrEmpty(strY)) { strY = "0"; }

            int x = int.Parse(strX);
            int y = int.Parse(strY);

            if(y>0 && x > 0) { output = "1"; }
            else if(y<0 && x>0) { output = "4"; }
            else if(y>0 && x<0) { output = "2"; }
            else if(x == 0 && y == 0) { output = "No quadrant, both axis are 0."; }
            else if(x<0 && y<0){ output = "3"; }
            else { output = "Value of X or Y is zero"; }

            outputMessage.Text = output;
        }


    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div>
            <asp:Label runat="server" >x value : </asp:Label>
            <asp:TextBox runat="server" ID="xValue" name="xValue" placeholder="Write x Value"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="reqX" ControlToValidate="xValue" ErrorMessage="you must enter the value for x"></asp:RequiredFieldValidator>
            <br />
            <asp:Label runat="server">y value : </asp:Label>
            <asp:TextBox runat="server" ID="yValue" name="yValue" placeholder="Write y Value"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="reqY" ControlToValidate="yValue" ErrorMessage="you must enter the value for y"></asp:RequiredFieldValidator>
            <br />
            <asp:Button Text="Submit" runat="server" ID="submitButton" />
        </div>
        <p>Your quadrant is: <asp:Label runat="server" id="outputMessage" /></p>
    </form>

   

</body>
</html>
